#include <stdio.h>
#include <list>
#include <vector>

using namespace std;

void processTask(int taskNum) {
    int money = 0;
    int itemsCount = 0;

    scanf("%d", &money);
    scanf("%d", &itemsCount);

    vector<int> items(itemsCount);
    list<int> items_nums;
    for (int i = 0; i < itemsCount; ++i) {
        scanf("%d", &items[i]);
        if (items[i] < money) {
            items_nums.push_back(i);
        }
    }
    int solution[] = {0, 0};
    int best_score = 0;
    for (auto iter = items_nums.begin(); iter != items_nums.end(); ++iter) {
        for (auto jiter = next(iter); jiter != items_nums.end(); ++jiter) {
            int sum = items[*iter] + items[*jiter];
            if (sum > best_score && sum <= money) {
                best_score = sum;
                solution[0] = *iter;
                solution[1] = *jiter;
            }
        }
    }

    printf("Case #%d: %d %d\n", taskNum+1, solution[0]+1, solution[1]+1);
}

int main() {
    int N = 0;
    scanf("%d", &N);
    for (int i = 0; i < N; ++i) {
        processTask(i);
    }
}
