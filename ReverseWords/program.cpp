#include <stdio.h>
#include <string>
#include <iostream>
#include <list>
#include <memory>
#include <sstream>

using namespace std;

shared_ptr<list<string> > splitString(string& source, char delim){
    list<string> result;
    stringstream stream(source);
    string item;
    while(getline(stream, item, delim)) {
        result.push_back(item);
    }
    return make_shared<list <string> >(result);
}

void processTask(int taskNum) {
    string input;
    getline(cin, input);    
    auto items = splitString(input, ' ');
    cout << "Case #" << taskNum + 1 << ":";
    for (auto it = items->rbegin(); it != items->rend(); ++it) {
        cout << " " << *it;
    }
    cout << endl;
}

int main() {
    int N = 0;
    scanf("%d\n", &N);
    for (int i = 0; i < N; ++i) {
        processTask(i);
    }
}
