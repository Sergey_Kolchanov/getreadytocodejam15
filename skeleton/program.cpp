#include <stdio.h>

void processTask(int taskNum) {
    printf("Case #%d: \n", taskNum+1);
}

int main() {
    int N = 0;
    scanf("%d", &N);
    for (int i = 0; i < N; ++i) {
        processTask(i);
    }
}
