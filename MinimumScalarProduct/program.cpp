#include <stdio.h>
#include <iostream>
#include <vector>
#include <inttypes.h>
#include <utility>
#include <climits>
#include <algorithm>

using namespace std;

int64_t vector_mult(const vector<int64_t>& v1, const vector<int64_t>& v2) {
    int len = v1.size();
    int64_t res = 0;
    for (int i = 0; i < len; ++i) {
        res += v1[i] * v2[i];
    }
    return res;
}

void processTask(int taskNum) {
    int len = 0;
    cin >> len;
    vector<int64_t> v1(len);
    vector<int64_t> v2(len);
    for (int i = 0; i < len; ++i) {
        cin >> v1[i];
    }
    for (int i = 0; i < len; ++i) {
        cin >> v2[i];
    }
    sort(v1.begin(), v1.end());
    sort(v2.begin(), v2.end(), greater<int64_t>());
    int64_t res = vector_mult(v1, v2);
    printf("Case #%d: %ld\n", taskNum+1, res);
}

int main() {
    int N = 0;
    scanf("%d", &N);
    for (int i = 0; i < N; ++i) {
        processTask(i);
    }
}
